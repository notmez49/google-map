import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      src: "/src",
      "@components": "/src/components",
      "@modules": "/src/modules"
    },
  },
  server: {
    open: true,
    port: '8080',
    proxy: {
      '/autocomplete': {
        target: 'https://maps.googleapis.com/maps/api/place/autocomplete/json?types=establishment&radius=500',
        changeOrigin: true,
        rewrite: (path) => {
          return path.replace(/^\/autocomplete/, '')
        },
      },
      '/placedetails': {
        target: 'https://maps.googleapis.com/maps/api/place/details/json?radius=500',
        changeOrigin: true,
        rewrite: (path) => {
          return path.replace(/^\/placedetails/, '')
        },
      },
    },
    cors: true
  },
})

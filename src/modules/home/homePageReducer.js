import { createReducer } from '@reduxjs/toolkit'
import { map } from 'lodash'

export const FETCH_ADDRESS = 'FETCH_ADDRESS';
export const FETCH_ADDRESS_FULFILLED = 'FETCH_ADDRESS_FULFILLED';
export const API_ERROR = 'API_ERROR';
export const GET_PLACE_DETAILS = 'GET_PLACE_DETAILS';
export const GET_PLACE_DETAILS_COMPLETE = 'GET_PLACE_DETAILS_COMPLETE';
export const LISTEN_AUTO_COMPLETE = 'LISTEN_AUTO_COMPLETE';
export const DISMISS_ERROR_MESSAGE = 'DISMISS_ERROR_MESSAGE';

export const fetchAddress = address => ({ type: FETCH_ADDRESS, payload: address });
export const fetchAddressFulfilled = payload => {
    const isSuccess = payload.status === 'OK'
    return isSuccess ? ({ type: FETCH_ADDRESS_FULFILLED, payload }) : ({ type: API_ERROR, payload })
};
export const getPlaceDetails = payload => ({ type: GET_PLACE_DETAILS, payload });
export const getPlaceDetailsComplete = payload => ({ type: GET_PLACE_DETAILS_COMPLETE, payload });
export const removeErrorMessage = payload => ({ type: DISMISS_ERROR_MESSAGE, payload });

const initialState = {
    data: [],
    loading_data: false,
    place_details: null,
    error_message: null
}

const HomeReducer = createReducer(initialState, {
    [FETCH_ADDRESS]: state => {
        state.loading_data = true
    },
    [FETCH_ADDRESS_FULFILLED]: (state, { payload }) => {
        const list = map(payload.predictions, (res) => {
            return {
                label: res.description,
                place_id: res.place_id,
                value: res.description
            }
        })
        state.loading_data = false
        state.data = list
    },
    [GET_PLACE_DETAILS_COMPLETE]: (state, { payload }) => {
        state.place_details = payload.result
    },
    [API_ERROR]: (state, { payload }) => {
        state.error_message = payload.status + ' : ' + payload.error_message
    },
    [DISMISS_ERROR_MESSAGE]: (state) => {
        state.error_message = null
    }
})

export default HomeReducer
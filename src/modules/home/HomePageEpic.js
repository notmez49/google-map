// import { ofType, delay, mapTo  } from 'redux-observable';
import { ofType } from 'redux-observable';
import { mergeMap, map, filter, mapTo, debounceTime } from 'rxjs'
import { FETCH_ADDRESS, LISTEN_AUTO_COMPLETE, fetchAddressFulfilled, GET_PLACE_DETAILS, getPlaceDetailsComplete } from '@modules/home/HomePageReducer'
import { ajax } from 'rxjs/ajax';
import { GOOGLE_MAP_AUTOCOMPLETE_URL, GOOGLE_MAP_PLACE_DETAILS_URL } from 'src/apiPathConstants';
import {
   mock_suggestion_list,
   mock_place_details
} from './mockData'
const mock = false;
const getSuggestedData = (action) => {
   const url = mock ? 'https://catfact.ninja/fact' : GOOGLE_MAP_AUTOCOMPLETE_URL.replace('%textInput%', action.payload);
   return ajax({
      url: url,
      crossDomain: true,
      createXHR: function () {
         return new XMLHttpRequest();
      }
   })
}

const getPlaceInfo = (action) => {
   const url = mock ? 'https://catfact.ninja/fact' : GOOGLE_MAP_PLACE_DETAILS_URL.replace('%textInput%', action.payload);
   return ajax({
      url: url,
      crossDomain: true,
      createXHR: function () {
         return new XMLHttpRequest();
      }
   })
}

const listenAutoCompleteEpic = action$ => action$.pipe(
   filter(action => action.type === LISTEN_AUTO_COMPLETE),
   mapTo({ type: FETCH_ADDRESS }),
);


const fetchAddressEpic = action$ => action$.pipe(
   ofType(FETCH_ADDRESS),
   debounceTime(2000),
   mergeMap(action =>
      getSuggestedData(action).pipe(
         map(res => fetchAddressFulfilled(mock ? mock_suggestion_list : res.response))
      )
   ),
);

const getPlaceDetailEpic = action$ => action$.pipe(
   ofType(GET_PLACE_DETAILS),
   mergeMap(action =>
      getPlaceInfo(action).pipe(
         map(res => getPlaceDetailsComplete(mock ? mock_place_details : res.response))
      )
   ),
);

export {
   fetchAddressEpic,
   listenAutoCompleteEpic,
   getPlaceDetailEpic
}


import { createSelector } from '@reduxjs/toolkit'

export const HomePageSelector = state => state.HomePageReducer
export const getSuggestionList = ()=> createSelector(HomePageSelector, res=> res.data)
export const getLoadingMoreData = ()=> createSelector(HomePageSelector, res=> res.loading_data)
export const getPlaceDetailSelector = ()=> createSelector(HomePageSelector, res=> res.place_details)
export const getApiErrorMessage = ()=> createSelector(HomePageSelector, res=> res.error_message)
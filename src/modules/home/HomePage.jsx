import { useState } from 'react'
import classes from './HomePage.module.css'
import { useDispatch, useSelector } from 'react-redux'
import { fetchAddress, getPlaceDetails, removeErrorMessage } from './HomePageReducer'
import { AutoComplete, Input, notification } from 'antd';
import GoogleMapReact from 'google-map-react';
import { find, concat } from 'lodash';
import { LoadingOutlined } from '@ant-design/icons';
import { useEffect } from 'react';
import { getSuggestionList, getLoadingMoreData, getPlaceDetailSelector, getApiErrorMessage } from './HomePageSelector'
const renderTitle = () => (
  <div style={{ textAlign: 'center' }}>
    <LoadingOutlined />
  </div>
);

const HomePage = () => {
  const defaultProps = {
    center: {
      lat: 3.159206133223204,
      lng: 101.71655950000002
    },
    zoom: 11
  };
  const dispatch = useDispatch()
  const suggest_address = useSelector(getSuggestionList())
  const is_loading_more_data = useSelector(getLoadingMoreData())
  const place_details = useSelector(getPlaceDetailSelector())
  const api_error_message = useSelector(getApiErrorMessage())

  const [global_map, setGlobalMap] = useState(null)
  const [global_maps, setGlobalMaps] = useState(null)

  const loading_label = is_loading_more_data ? [{
    label: renderTitle('Loading More Data....'),
  }] : []
  const [selected_place, setSelectedPlace] = useState(null)
  const triggerSearch = (e) => {
    const skip_search = find(suggest_address, (res)=> res.label === e)
    if (skip_search){
      return;
    }
    dispatch(fetchAddress(e));
  }
  useEffect(() => {
    if (selected_place) {
      dispatch(getPlaceDetails(selected_place.place_id));
    }
  }, [dispatch, selected_place])

  useEffect(() => {
    if (api_error_message) {
      notification.error({
        placement: 'bottom',
        bottom: 50,
        rtl: true,
        message: `API error`,
        description: api_error_message,
        onClose: ()=> dispatch(removeErrorMessage())
      });
      // dispatch(getPlaceDetails(selected_place.place_id));
    }
  }, [api_error_message, dispatch])


  useEffect(() => {
    if (place_details && global_map) {
      global_map.setCenter(place_details.geometry.location);
      const marker = new global_maps.Marker({ map: global_map });
      const infoWindow = new global_maps.InfoWindow({
        content: getInfoWindowString(place_details),
      })
      marker.setPlace({
        placeId: place_details.place_id,
        location: place_details.geometry.location
      });
      marker.addListener("click", () => {
        infoWindow.open(global_map, marker);
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [place_details])

  const getInfoWindowString = (place) => `
    <div style="color: #000;">
      <div style="font-size: 16px;">
        ${place?.adr_address}
      </div>
      <div style="font-size: 14px;">
        <span style="color: grey;">
        ${place?.rating}
        </span>
        <span style="color: orange;">${String.fromCharCode(9733).repeat(Math.floor(place.rating))}</span><span style="color: lightgrey;">${String.fromCharCode(9733).repeat(5 - Math.floor(place.rating))}</span>
      </div>
      <div style="font-size: 14px; color: grey;">
        ${place?.types[0]}
      </div>
      <div style="font-size: 14px; color: grey;">
        ${'$'.repeat(place?.price_level)}
      </div>
      <div style="font-size: 14px; color: green;">
        ${place?.opening_hours?.open_now ? 'Open' : 'Closed'}
      </div>
    </div>`;

  const handleApiLoaded = (map, maps) => {
    setGlobalMap(map)
    setGlobalMaps(maps)
  };

  const selectPlace = (place_id) => {
    if (!place_id) {
      return;
    }
    let place_object = find(suggest_address, (res) => res.value === place_id)
    setSelectedPlace(place_object)
  }

  return (
    <>
      <AutoComplete
        popupClassName={classes.suggestionList}
        className={classes.autoComplete}
        defaultOpen={true}
        onChange={triggerSearch}
        options={concat(suggest_address, loading_label)}
        onSelect={selectPlace}
        onSearch={selectPlace}
      >
        <Input.Search size="large" placeholder="Search" />
      </AutoComplete>
      <div className={classes.mapContainer}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: import.meta.env.VITE_GOOGLE_MAP_KEY }}
          defaultCenter={defaultProps.center}
          defaultZoom={defaultProps.zoom}
          yesIWantToUseGoogleMapApiInternals
          onGoogleApiLoaded={({ map, maps }) => handleApiLoaded(map, maps, [])}
        >
        </GoogleMapReact>
      </div>
    </>
  )
}

export default HomePage

let mock_suggestion_list = {
    "predictions": [
        {
            "description": "KLCC, Kuala Lumpur City Centre, Kuala Lumpur, Federal Territory of Kuala Lumpur, Malaysia",
            "matched_substrings": [
                {
                    "length": 4,
                    "offset": 0
                }
            ],
            "place_id": "ChIJH5xmLdE3zDERKa4a_IywVck",
            "reference": "ChIJH5xmLdE3zDERKa4a_IywVck",
            "structured_formatting": {
                "main_text": "KLCC",
                "main_text_matched_substrings": [
                    {
                        "length": 4,
                        "offset": 0
                    }
                ],
                "secondary_text": "Kuala Lumpur City Centre, Kuala Lumpur, Federal Territory of Kuala Lumpur, Malaysia"
            },
            "terms": [
                {
                    "offset": 0,
                    "value": "KLCC"
                },
                {
                    "offset": 6,
                    "value": "Kuala Lumpur City Centre"
                },
                {
                    "offset": 32,
                    "value": "Kuala Lumpur"
                },
                {
                    "offset": 46,
                    "value": "Federal Territory of Kuala Lumpur"
                },
                {
                    "offset": 81,
                    "value": "Malaysia"
                }
            ],
            "types": [
                "shopping_mall",
                "point_of_interest",
                "establishment"
            ]
        },
        {
            "description": "KLCC Park, Kuala Lumpur City Centre, Kuala Lumpur, Federal Territory of Kuala Lumpur, Malaysia",
            "matched_substrings": [
                {
                    "length": 4,
                    "offset": 0
                }
            ],
            "place_id": "ChIJBWbm2tM3zDERTno0px940s4",
            "reference": "ChIJBWbm2tM3zDERTno0px940s4",
            "structured_formatting": {
                "main_text": "KLCC Park",
                "main_text_matched_substrings": [
                    {
                        "length": 4,
                        "offset": 0
                    }
                ],
                "secondary_text": "Kuala Lumpur City Centre, Kuala Lumpur, Federal Territory of Kuala Lumpur, Malaysia"
            },
            "terms": [
                {
                    "offset": 0,
                    "value": "KLCC Park"
                },
                {
                    "offset": 11,
                    "value": "Kuala Lumpur City Centre"
                },
                {
                    "offset": 37,
                    "value": "Kuala Lumpur"
                },
                {
                    "offset": 51,
                    "value": "Federal Territory of Kuala Lumpur"
                },
                {
                    "offset": 86,
                    "value": "Malaysia"
                }
            ],
            "types": [
                "tourist_attraction",
                "park",
                "point_of_interest",
                "establishment"
            ]
        },
        {
            "description": "KLC Consulting Engineering, San Pablo Avenue, El Cerrito, CA, USA",
            "matched_substrings": [
                {
                    "length": 26,
                    "offset": 0
                }
            ],
            "place_id": "ChIJp4UvhP94hYARwDvUvWcfvg8",
            "reference": "ChIJp4UvhP94hYARwDvUvWcfvg8",
            "structured_formatting": {
                "main_text": "KLC Consulting Engineering",
                "main_text_matched_substrings": [
                    {
                        "length": 26,
                        "offset": 0
                    }
                ],
                "secondary_text": "San Pablo Avenue, El Cerrito, CA, USA"
            },
            "terms": [
                {
                    "offset": 0,
                    "value": "KLC Consulting Engineering"
                },
                {
                    "offset": 28,
                    "value": "San Pablo Avenue"
                },
                {
                    "offset": 46,
                    "value": "El Cerrito"
                },
                {
                    "offset": 58,
                    "value": "CA"
                },
                {
                    "offset": 62,
                    "value": "USA"
                }
            ],
            "types": [
                "point_of_interest",
                "establishment"
            ]
        },
        {
            "description": "KLCC Convention Centre Loading Bay Checkpoint, Persiaran KLCC, Kuala Lumpur, Federal Territory of Kuala Lumpur, Malaysia",
            "matched_substrings": [
                {
                    "length": 4,
                    "offset": 0
                }
            ],
            "place_id": "ChIJATaCWGU3zDER32m__CAwDyY",
            "reference": "ChIJATaCWGU3zDER32m__CAwDyY",
            "structured_formatting": {
                "main_text": "KLCC Convention Centre Loading Bay Checkpoint",
                "main_text_matched_substrings": [
                    {
                        "length": 4,
                        "offset": 0
                    }
                ],
                "secondary_text": "Persiaran KLCC, Kuala Lumpur, Federal Territory of Kuala Lumpur, Malaysia"
            },
            "terms": [
                {
                    "offset": 0,
                    "value": "KLCC Convention Centre Loading Bay Checkpoint"
                },
                {
                    "offset": 47,
                    "value": "Persiaran KLCC"
                },
                {
                    "offset": 63,
                    "value": "Kuala Lumpur"
                },
                {
                    "offset": 77,
                    "value": "Federal Territory of Kuala Lumpur"
                },
                {
                    "offset": 112,
                    "value": "Malaysia"
                }
            ],
            "types": [
                "point_of_interest",
                "establishment"
            ]
        },
        {
            "description": "KLCC, West 8th Avenue, Eugene, OR, USA",
            "matched_substrings": [
                {
                    "length": 4,
                    "offset": 0
                }
            ],
            "place_id": "ChIJ3xJxvDkfwVQRHC9xoeLPV9Y",
            "reference": "ChIJ3xJxvDkfwVQRHC9xoeLPV9Y",
            "structured_formatting": {
                "main_text": "KLCC",
                "main_text_matched_substrings": [
                    {
                        "length": 4,
                        "offset": 0
                    }
                ],
                "secondary_text": "West 8th Avenue, Eugene, OR, USA"
            },
            "terms": [
                {
                    "offset": 0,
                    "value": "KLCC"
                },
                {
                    "offset": 6,
                    "value": "West 8th Avenue"
                },
                {
                    "offset": 23,
                    "value": "Eugene"
                },
                {
                    "offset": 31,
                    "value": "OR"
                },
                {
                    "offset": 35,
                    "value": "USA"
                }
            ],
            "types": [
                "point_of_interest",
                "establishment"
            ]
        }
    ],
    "status": "OK"
}
let mock_place_details = {
    "html_attributions" : [],
    "result" : {
       "address_components" : [
          {
             "long_name" : "241",
             "short_name" : "241",
             "types" : [ "subpremise" ]
          },
          {
             "long_name" : "Menara Berkembar Petronas",
             "short_name" : "Menara Berkembar Petronas",
             "types" : [ "premise" ]
          },
          {
             "long_name" : "Kuala Lumpur City Centre",
             "short_name" : "Kuala Lumpur City Centre",
             "types" : [ "sublocality_level_1", "sublocality", "political" ]
          },
          {
             "long_name" : "Kuala Lumpur",
             "short_name" : "Kuala Lumpur",
             "types" : [ "locality", "political" ]
          },
          {
             "long_name" : "Wilayah Persekutuan Kuala Lumpur",
             "short_name" : "Wilayah Persekutuan Kuala Lumpur",
             "types" : [ "administrative_area_level_1", "political" ]
          },
          {
             "long_name" : "Malaysia",
             "short_name" : "MY",
             "types" : [ "country", "political" ]
          },
          {
             "long_name" : "50088",
             "short_name" : "50088",
             "types" : [ "postal_code" ]
          }
       ],
       "adr_address" : "241, Menara Berkembar Petronas, \u003cspan class=\"extended-address\"\u003eKuala Lumpur City Centre\u003c/span\u003e, \u003cspan class=\"postal-code\"\u003e50088\u003c/span\u003e \u003cspan class=\"locality\"\u003eKuala Lumpur\u003c/span\u003e, \u003cspan class=\"region\"\u003eWilayah Persekutuan Kuala Lumpur\u003c/span\u003e, \u003cspan class=\"country-name\"\u003eMalaysia\u003c/span\u003e",
       "business_status" : "OPERATIONAL",
       "current_opening_hours" : {
          "open_now" : false,
          "periods" : [
             {
                "close" : {
                   "date" : "2023-06-25",
                   "day" : 0,
                   "time" : "2200"
                },
                "open" : {
                   "date" : "2023-06-25",
                   "day" : 0,
                   "time" : "1000"
                }
             },
             {
                "close" : {
                   "date" : "2023-06-26",
                   "day" : 1,
                   "time" : "2200"
                },
                "open" : {
                   "date" : "2023-06-26",
                   "day" : 1,
                   "time" : "1000"
                }
             },
             {
                "close" : {
                   "date" : "2023-06-27",
                   "day" : 2,
                   "time" : "2200"
                },
                "open" : {
                   "date" : "2023-06-27",
                   "day" : 2,
                   "time" : "1000"
                }
             },
             {
                "close" : {
                   "date" : "2023-06-28",
                   "day" : 3,
                   "time" : "2200"
                },
                "open" : {
                   "date" : "2023-06-28",
                   "day" : 3,
                   "time" : "1000"
                }
             },
             {
                "close" : {
                   "date" : "2023-06-29",
                   "day" : 4,
                   "time" : "2200"
                },
                "open" : {
                   "date" : "2023-06-29",
                   "day" : 4,
                   "time" : "1000"
                }
             },
             {
                "close" : {
                   "date" : "2023-06-30",
                   "day" : 5,
                   "time" : "2200"
                },
                "open" : {
                   "date" : "2023-06-30",
                   "day" : 5,
                   "time" : "1000"
                }
             },
             {
                "close" : {
                   "date" : "2023-06-24",
                   "day" : 6,
                   "time" : "2200"
                },
                "open" : {
                   "date" : "2023-06-24",
                   "day" : 6,
                   "time" : "1000"
                }
             }
          ],
          "weekday_text" : [
             "Monday: 10:00\u202fam\u2009\u2013\u200910:00\u202fpm",
             "Tuesday: 10:00\u202fam\u2009\u2013\u200910:00\u202fpm",
             "Wednesday: 10:00\u202fam\u2009\u2013\u200910:00\u202fpm",
             "Thursday: 10:00\u202fam\u2009\u2013\u200910:00\u202fpm",
             "Friday: 10:00\u202fam\u2009\u2013\u200910:00\u202fpm",
             "Saturday: 10:00\u202fam\u2009\u2013\u200910:00\u202fpm",
             "Sunday: 10:00\u202fam\u2009\u2013\u200910:00\u202fpm"
          ]
       },
       "editorial_summary" : {
          "language" : "en",
          "overview" : "Opened in 1998, this 6-storey shopping hub offers designer boutiques & international restaurants."
       },
       "formatted_address" : "241, Menara Berkembar Petronas, Kuala Lumpur City Centre, 50088 Kuala Lumpur, Wilayah Persekutuan Kuala Lumpur, Malaysia",
       "formatted_phone_number" : "03-2382 2828",
       "geometry" : {
          "location" : {
             "lat" : 3.1577167,
             "lng" : 101.7121716
          },
          "viewport" : {
             "northeast" : {
                "lat" : 3.1593166,
                "lng" : 101.7149082
             },
             "southwest" : {
                "lat" : 3.156100200000001,
                "lng" : 101.709693
             }
          }
       },
       "icon" : "https://maps.gstatic.com/mapfiles/place_api/icons/v1/png_71/shopping-71.png",
       "icon_background_color" : "#4B96F3",
       "icon_mask_base_uri" : "https://maps.gstatic.com/mapfiles/place_api/icons/v2/shopping_pinlet",
       "international_phone_number" : "+60 3-2382 2828",
       "name" : "Suria KLCC",
       "opening_hours" : {
          "open_now" : false,
          "periods" : [
             {
                "close" : {
                   "day" : 0,
                   "time" : "2200"
                },
                "open" : {
                   "day" : 0,
                   "time" : "1000"
                }
             },
             {
                "close" : {
                   "day" : 1,
                   "time" : "2200"
                },
                "open" : {
                   "day" : 1,
                   "time" : "1000"
                }
             },
             {
                "close" : {
                   "day" : 2,
                   "time" : "2200"
                },
                "open" : {
                   "day" : 2,
                   "time" : "1000"
                }
             },
             {
                "close" : {
                   "day" : 3,
                   "time" : "2200"
                },
                "open" : {
                   "day" : 3,
                   "time" : "1000"
                }
             },
             {
                "close" : {
                   "day" : 4,
                   "time" : "2200"
                },
                "open" : {
                   "day" : 4,
                   "time" : "1000"
                }
             },
             {
                "close" : {
                   "day" : 5,
                   "time" : "2200"
                },
                "open" : {
                   "day" : 5,
                   "time" : "1000"
                }
             },
             {
                "close" : {
                   "day" : 6,
                   "time" : "2200"
                },
                "open" : {
                   "day" : 6,
                   "time" : "1000"
                }
             }
          ],
          "weekday_text" : [
             "Monday: 10:00\u202fam\u2009\u2013\u200910:00\u202fpm",
             "Tuesday: 10:00\u202fam\u2009\u2013\u200910:00\u202fpm",
             "Wednesday: 10:00\u202fam\u2009\u2013\u200910:00\u202fpm",
             "Thursday: 10:00\u202fam\u2009\u2013\u200910:00\u202fpm",
             "Friday: 10:00\u202fam\u2009\u2013\u200910:00\u202fpm",
             "Saturday: 10:00\u202fam\u2009\u2013\u200910:00\u202fpm",
             "Sunday: 10:00\u202fam\u2009\u2013\u200910:00\u202fpm"
          ]
       },
       "photos" : [
          {
             "height" : 2860,
             "html_attributions" : [
                "\u003ca href=\"https://maps.google.com/maps/contrib/102537973860340545171\"\u003eGV View\u003c/a\u003e"
             ],
             "photo_reference" : "AZose0nSAQn78nFuhQQarOBsBvQ1ezw5kS8JcHK-iir7l0GQUYGXBQGtk0k5WwdUEAb3wTmh5chRV8MzZ0Gz59qlfN593JJSRFkEO8FmSP31MeiQH6RGh2UA53X19uqfoVVlvBCcpX-quHVGCOaQ2BwdnGZ_fFw7PNgxc467KUo0q19KbjIK",
             "width" : 3814
          },
          {
             "height" : 3000,
             "html_attributions" : [
                "\u003ca href=\"https://maps.google.com/maps/contrib/116403383634312883576\"\u003eabu dirhamsyah\u003c/a\u003e"
             ],
             "photo_reference" : "AZose0k1zQYv99pbrvLOaQdrHDADT4GUV5JH2TbRPTB_pLkFsAZbwhdDkRAqUygSr7FL03oWgcYUd1K3W2TWEPFynN5wLSfNdm_YuM2I-0dTrZzFj9XAlZ_MsXNsVYuqSqsadzQ7-oRtKWwtECdhoLVNC5kOhL2CWRhxcErdHGxf1Q6m1GhW",
             "width" : 4000
          },
          {
             "height" : 3024,
             "html_attributions" : [
                "\u003ca href=\"https://maps.google.com/maps/contrib/115818682511885107019\"\u003eFawaaz N\u003c/a\u003e"
             ],
             "photo_reference" : "AZose0lGYURiEYaXWmdG2mgeWzdr-y7o1QNUQSQhmrOJBB3v1xOShZAHAstO3CBdCmCP3YBaAYNDlWPPk33FO_ui7S5hhYbnmcfxvliFBQCXP3SQmDLVjOHphPWiD-M1LCapwS7DtPL0Lq-BhzFbEu18VMazDr5XmPfaS1SYnnncHHpQoAn8",
             "width" : 4032
          },
          {
             "height" : 2268,
             "html_attributions" : [
                "\u003ca href=\"https://maps.google.com/maps/contrib/106000836039450944221\"\u003e\u5f35\u4e00\u4ec1\u003c/a\u003e"
             ],
             "photo_reference" : "AZose0m2D_7IaF2Jbhk2zwd_MlEdkmHFbOZtaimsTo13d0_1olqkgaGbrRos8FsZ6UkiI0iFoAbh1y8ClZ0EsAWKs9df49HVnVPwMzn4laxhK4Y1pZes0LsC1DOcEilISSCHJ3XGuwC9xBju4QdQgH0tiEA40AG30IvemHDTPSN_VPxcgMJI",
             "width" : 4032
          },
          {
             "height" : 4032,
             "html_attributions" : [
                "\u003ca href=\"https://maps.google.com/maps/contrib/109852622610401060880\"\u003eRachmat H\u003c/a\u003e"
             ],
             "photo_reference" : "AZose0lccj7U51WX4Ul56ZOkgXwrih9p3i4qkxIYXpzC1H44_VrRF8B-1Lr1dlYTyWNsxJ69aOOrCuEbOzXkw26YUIxw_ck5ggeYDKAe1v3QeqImVCUlOy-7XOUBJ68nZLNgWcS4CfKppBfjt0n4-KjZ2AxPBZgviJAgZWKI3bEt5-4-GErh",
             "width" : 3024
          },
          {
             "height" : 689,
             "html_attributions" : [
                "\u003ca href=\"https://maps.google.com/maps/contrib/111950755063787619814\"\u003eJoseph Tzidikmann\u003c/a\u003e"
             ],
             "photo_reference" : "AZose0muosN4W3XPG2IR4DWU7K1PJQCfpLqdYfvOKGHoOgpkxWoFaoyOKFUAaz4PY6n_nUrKTJJeY2Qzc6-Nr1N3GvAA3MGX7tUHpw0I3We6vNIkNJNsG8FBE6wt6GXXJWgz-Z7HBFGD1i2k2QCV6-_W3XqL689VSt4DZvct1w6orVSV-1ra",
             "width" : 914
          },
          {
             "height" : 2268,
             "html_attributions" : [
                "\u003ca href=\"https://maps.google.com/maps/contrib/107185790690774171823\"\u003eShadow Weaver\u003c/a\u003e"
             ],
             "photo_reference" : "AZose0lBP0uH984OkTdamShaXLGXENtryaarucDyPkswgYfuZ8-eryIW4sPWhbcdPsq9i9C5NSUl8563VQVLJElJifQ8watPgIJfmX6CGoHgxrRPmPq6NKex1RYAVb4JFCgBPOWFYGztDPE2wCDWorHaUeDFjHISODNiyWBNbkOgGqT6H8cl",
             "width" : 4032
          },
          {
             "height" : 4160,
             "html_attributions" : [
                "\u003ca href=\"https://maps.google.com/maps/contrib/118418622459421077723\"\u003eDesmond Gan say how\u003c/a\u003e"
             ],
             "photo_reference" : "AZose0n6hXMle2ClxNvPsq_NPEFgtIYhLtBH3HXcJQHdpG_I0GhsTelWJvvCcIBO3dK9UeoQx2alOadqp40G9hUNoaCHfilSM0OEvOEDrdLijH-5mrZ0kDCnHUR43waegfSKYUCmOGlAQrO-spHJCuC1B4A4et33hrAbQMFw8rVL065GqJUB",
             "width" : 3120
          },
          {
             "height" : 4032,
             "html_attributions" : [
                "\u003ca href=\"https://maps.google.com/maps/contrib/104412518803175061479\"\u003eAnastasia Holz\u003c/a\u003e"
             ],
             "photo_reference" : "AZose0mFlYOwRoy2ntq8QvRFlePvFAOhL2XfxizGu9ovIX8Io6YbP96k8AktvKkCa5_DgdCltOPdndSAI6aiqCaKOr2iwMjP0uTUBqUjYQJj1M0QCU7byqqkXjwciTLFrs_OdshGHKX_jkTUytX7TJbyZC1IzdQq9x7ZMiKE731lPrbQcpTR",
             "width" : 3024
          },
          {
             "height" : 3024,
             "html_attributions" : [
                "\u003ca href=\"https://maps.google.com/maps/contrib/115558442660722949576\"\u003eMuh Fuei\u003c/a\u003e"
             ],
             "photo_reference" : "AZose0n81pRCZpAvo5FRjhcIQhnovuWx404FRBQwcIunF7odKO7jC9dSIsFKM7O0g-vcnXHVl2YMOaxemI6OHeM7pD6XIEttQH_Rv6VxCwwppTc00up7GONOgZtBE-zQGs-Kz63en7Q73sv9sUMFgXBBMQvI8Wxgr8o2rHOgXkof77LM24mV",
             "width" : 4032
          }
       ],
       "place_id" : "ChIJH5xmLdE3zDERKa4a_IywVck",
       "plus_code" : {
          "compound_code" : "5P56+3V Kuala Lumpur, Federal Territory of Kuala Lumpur, Malaysia",
          "global_code" : "6PM35P56+3V"
       },
       "rating" : 4.5,
       "reference" : "ChIJH5xmLdE3zDERKa4a_IywVck",
       "reviews" : [
          {
             "author_name" : "Muhamed Hannan",
             "author_url" : "https://www.google.com/maps/contrib/108722494487938624650/reviews",
             "language" : "en",
             "original_language" : "en",
             "profile_photo_url" : "https://lh3.googleusercontent.com/a-/AD_cMMQeD6mSokz2Ni5PzNfIPnyGMXVmkA_wuoPppkODzg4=s128-c0x00000000-cc-rp-mo-ba3",
             "rating" : 5,
             "relative_time_description" : "a month ago",
             "text" : "As its reputation stands. This is a lovely place to visit for a tourist photography experience.\nLovely fountain spots.\nGreat building view.\nBe it your first time or the 10th time the feel of the place largely remains the same.\nExperiencing the towers from the garden in front or the fountain area is lovely.\nWas lucky enough to catch the River of Life show on my last visit here (videos attached), where the fountain water dances along with the music :).\nPleasant experience.",
             "time" : 1683915915,
             "translated" : false
          },
          {
             "author_name" : "Md Kazi Masum",
             "author_url" : "https://www.google.com/maps/contrib/103170150476794721513/reviews",
             "language" : "en",
             "original_language" : "en",
             "profile_photo_url" : "https://lh3.googleusercontent.com/a-/AD_cMMT5uLIOPPtSDLE4lVvv2CRdfgnDM5QgcjwL9Yos4g=s128-c0x00000000-cc-rp-mo-ba6",
             "rating" : 5,
             "relative_time_description" : "5 months ago",
             "text" : "Lovely view from the mall's outside. In close proximity to the Petronas Twin Tower, the mall serves as the primary draw. Inside, there are a lot of eateries and stores. Since most visitors and locals come here to take a walk and enjoy the stunning view, it is frequently crowded, especially on the weekends. Even at night, it is rather bustling.",
             "time" : 1674623185,
             "translated" : false
          },
          {
             "author_name" : "A Kanjani",
             "author_url" : "https://www.google.com/maps/contrib/115779458996078824637/reviews",
             "language" : "en",
             "original_language" : "en",
             "profile_photo_url" : "https://lh3.googleusercontent.com/a-/AD_cMMTGDkAGAzI_VqmowgMiTAgIKibz7M-J_Lv59eCr=s128-c0x00000000-cc-rp-mo-ba7",
             "rating" : 5,
             "relative_time_description" : "9 months ago",
             "text" : "Awesome shopping center.\n\nIt\u2019s got all the brands under the sun weather it\u2019s shopping or food & beverage.\n\nIt\u2019s connected by a Pedestrian sky walk bridge leading directly to Pavilion Shopping center as well, which is Air Conditioned and approx a 15 mins walk.\n\nThe shopping center has a nice fountain outside in the open area and a large garden next to it.\n\nAnd of course the amazing view of the Petronas Twin Towers.\n\nRecommended \ud83c\udf1f\ud83c\udf1f\ud83c\udf1f\ud83c\udf1f\ud83c\udf1f",
             "time" : 1662007247,
             "translated" : false
          },
          {
             "author_name" : "Leanie Toh",
             "author_url" : "https://www.google.com/maps/contrib/113786156630007805436/reviews",
             "language" : "en",
             "original_language" : "en",
             "profile_photo_url" : "https://lh3.googleusercontent.com/a-/AD_cMMQZiWQRYg_SttZFVFTy3T5gDO9ywRkmhu4gkx5sCw=s128-c0x00000000-cc-rp-mo-ba6",
             "rating" : 5,
             "relative_time_description" : "5 months ago",
             "text" : "Lovely shopping mall with a park. Probably the most visited mall in town...for locals and tourists. No doubt it was planned and designed to perfection. You tend to spend the whole day shopping or window shopping. Great place to hangout for all ages.",
             "time" : 1672130134,
             "translated" : false
          },
          {
             "author_name" : "Ravish M",
             "author_url" : "https://www.google.com/maps/contrib/116138807654098240752/reviews",
             "language" : "en",
             "original_language" : "en",
             "profile_photo_url" : "https://lh3.googleusercontent.com/a/AAcHTtcJdJfx2oKVC_W9K77Aqtn4x5g9CNpGgAZG7DgcEw=s128-c0x00000000-cc-rp-mo-ba6",
             "rating" : 5,
             "relative_time_description" : "5 months ago",
             "text" : "Very nice place to hang around and window shopping even if you do not plan to buy something. There are many places to shop branded products, cosmetics, clothing, books, etc. Abundance of choices of food and drinks. But be prepared to pay a premium on food as compared to the outside places. Enjoy the fountains and great view of Petronas towers outside the place. A must visit place of Kuala Lumpur, Malaysia.",
             "time" : 1672876537,
             "translated" : false
          }
       ],
       "types" : [ "shopping_mall", "point_of_interest", "establishment" ],
       "url" : "https://maps.google.com/?cid=14507695894215437865",
       "user_ratings_total" : 66722,
       "utc_offset" : 480,
       "vicinity" : "241, Menara Berkembar Petronas, Kuala Lumpur City Centre, Kuala Lumpur",
       "website" : "https://www.suriaklcc.com.my/",
       "wheelchair_accessible_entrance" : true
    },
    "status" : "OK"
 }

export {
    mock_suggestion_list,
    mock_place_details
}
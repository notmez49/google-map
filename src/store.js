import { createEpicMiddleware } from 'redux-observable';
import { createStore, applyMiddleware } from '@reduxjs/toolkit'
import rootEpic from './epics';
import rootReducer from './reducers'
import {composeWithDevTools} from 'redux-devtools-extension';

const composeEnhancers = composeWithDevTools ({
  // Specify custom devTools options
});
const epicMiddleware = createEpicMiddleware();
const store = createStore(rootReducer, composeEnhancers(
    applyMiddleware(epicMiddleware),
    // other store enhancers if any
));
epicMiddleware.run(rootEpic);

export default store 
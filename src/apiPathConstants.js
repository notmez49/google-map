const GOOGLE_MAP_AUTOCOMPLETE_URL = `/autocomplete/location=3.159206133223204%2C101.71655950000002&radius=500&key=${import.meta.env.VITE_GOOGLE_MAP_KEY}&language=en&input=%textInput%`
const GOOGLE_MAP_PLACE_DETAILS_URL =  `/placedetails/radius=500&place_id=%textInput%&key=${import.meta.env.VITE_GOOGLE_MAP_KEY}`
    // GOOGLE_MAP_GET_URL,
export {
    GOOGLE_MAP_PLACE_DETAILS_URL,
    GOOGLE_MAP_AUTOCOMPLETE_URL
}
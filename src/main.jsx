import React from 'react'
import ReactDOM from 'react-dom/client'
import HomePage from '@modules/home'
import AppLayout from '@components/appLayout'
import './index.css'
import { Provider } from 'react-redux'
import store from './store'
ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Provider store={store}>
      <AppLayout>
        <HomePage />
      </AppLayout>
    </Provider>
  </React.StrictMode>,
)

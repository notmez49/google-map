import { combineEpics } from 'redux-observable';
import { fetchAddressEpic, listenAutoCompleteEpic, getPlaceDetailEpic } from '@modules/home/HomePageEpic';

export default combineEpics(fetchAddressEpic, listenAutoCompleteEpic, getPlaceDetailEpic);
import { combineReducers } from '@reduxjs/toolkit'
import HomePageReducer from '@modules/home/HomePageReducer'
const rootReducer = combineReducers({
    HomePageReducer,
})

export default rootReducer
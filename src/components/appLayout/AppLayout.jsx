import { Space, Layout, } from 'antd';
import { PropTypes } from "prop-types";
import classes from './AppLayout.module.css'
const { Header, Content } = Layout


const AppLayout = ({ children }) => {
  const googleKey = import.meta.env.VITE_GOOGLE_MAP_KEY
  return (
    <Space direction="vertical" className={classes.container} size={[0, 48]}>
      <Layout>
        <Header className={classes.headerStyle}>
          <div className={classes.headerText}>
            {googleKey === 'PUT_YOUR_GOOGLE_API_KEY_HERE' ? 'Please replace your google key at .env file' : 'Assessment Project'}
          </div>
        </Header>
        <Content className={classes.contentStyle}>{children}</Content>
      </Layout>
    </Space>
  )
};

AppLayout.propTypes = {
  children: PropTypes.object
}
export default AppLayout;
